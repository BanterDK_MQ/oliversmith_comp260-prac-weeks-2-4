﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour
{

    public int beeStartCount = 20;

    public BeeMove beePrefab;
    public GameObject player1_go;
    public GameObject player2_go;

    public Rect spawnRect;

    GameObject beeParent;

    public int beeTotalCount;

    private float beePeriod;
    public float minBeePeriod = 0.1f;
    public float maxBeePeriod = 2f;
    public float beeTimeCounter;

    

    // Use this for initialization
    void Start()
    {
        beeParent = new GameObject("Bee[Parent]");

        beePeriod = Random.Range(minBeePeriod, maxBeePeriod);

        for (int i = 0; i < beeStartCount; i++)
        {
            //Make bee
            BeeMove bee = Instantiate(beePrefab);
            //Attach to Bee Spawner parent
            bee.transform.parent = beeParent.transform;
            //Give the bee a name and number
            bee.gameObject.name = "Bee " + beeTotalCount;


            ///<summary>
            ///Spawn Bee at B, where B is a point between A and C
            ///where A is the min value, and C is technically the width/length of spawnRect.
            ///B randomly spawns between A and B
            ///</ summary >
            float x = spawnRect.xMin + Random.value * spawnRect.width;
            float y = spawnRect.yMin + Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);


            //Give Bee Targets
            bee.Player1Target = player1_go.transform;
            bee.Player2Target = player2_go.transform;

            beeTotalCount++; 

            //Error if SpawnRect.xmin/ymin/xmax/ymax == 0)
            if (spawnRect.xMin == 0 | spawnRect.xMax == 0 | spawnRect.yMax == 0 | spawnRect.yMin == 0)
            {
                Debug.LogError("Spawn Rect has missing values");
            }
        }
    }


    public void Update()
    {
        beeTimeCounter = beeTimeCounter + Time.deltaTime;
        //Debug.Log(beeTimeCounter);

        if(beeTimeCounter >= beePeriod)
        {
            Debug.Log("BeeSpawned");
            spawnBee();
            beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
            beeTimeCounter = 0;
        }
    }

    public void spawnBee()
    {
        //Make bee
        BeeMove bee = Instantiate(beePrefab);
        //Attach to Bee Spawner parent
        bee.transform.parent = beeParent.transform;
        //Give the bee a name and number
        bee.gameObject.name = "Bee " + beeTotalCount;


        ///<summary>
        ///Spawn Bee at B, where B is a point between A and C
        ///where A is the min value, and C is technically the width/length of spawnRect.
        ///B randomly spawns between A and B
        ///</ summary >
        float x = spawnRect.xMin + Random.value * spawnRect.width;
        float y = spawnRect.yMin + Random.value * spawnRect.height;
        bee.transform.position = new Vector2(x, y);

        //Give Bee Targets
        bee.Player1Target = player1_go.transform;
        bee.Player2Target = player2_go.transform;

        //Increase Bee Count
        beeTotalCount++;
    }
    

    public void destroyBees(Vector2 centre, float radius)
    {
        for (int i = 0; i < beeParent.transform.childCount; i++)
        {
            Transform child = beeParent.transform.GetChild(i);
            //convert child.pos to Vector 2
            Vector2 v = (Vector2)child.position - centre;

            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);

                //Bring down the beecount
                beeTotalCount--;
            }
        }
    }

    public void OnDrawGizmos()
    {


        //draw spawn Rectangle
        Gizmos.color = Color.green;
        //Draw
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMin, spawnRect.yMin),
            new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMax, spawnRect.yMin),
            new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMax, spawnRect.yMax),
            new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
            new Vector2(spawnRect.xMin, spawnRect.yMax),
            new Vector2(spawnRect.xMin, spawnRect.yMin));
    }
}
