﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Vector2 move;
    public Vector2 velocity;
    public Vector2 direction;

    public float maxSpeed = 5.0f;       //metre/s
    public float acceleration = 20.0f;   //metre/s/s
    public float brake = 15.0f;          //metre/s/s
    public float turnSpeed = 25f;        //degrees/s

    public float destroyRadius = 1.5f;

    private float forwards;
    private float speed;                //metre/s

    private BeeSpawner beeSpawner;

    void Start()
    {
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }



    // Update is called once per frame
    void Update()
    {

        //Get p1 their inputs
        //Assign WASD to player 1,using Axis
        if (gameObject.tag == "Player1")
        {
            //Get p1 their inputs
            forwards = Input.GetAxis("Vertical");
            calculateSpeed();


            // the horizontal axis controls the turn
            float turn = Input.GetAxis("Horizontal");

            //turn
            transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);


            if(Input.GetButtonDown("Fire"))
            {
                //Destory bee's
                beeSpawner.destroyBees(transform.position, destroyRadius);
                
            }
        }

        //Get p2 their inputs
        //Assign arrow keys to player 1,using Axis
        if (gameObject.tag == "Player2")
        {
            forwards = Input.GetAxis("Vertical2");
            calculateSpeed();

            // the horizontal axis controls the turn
            float turn = Input.GetAxis("Horizontal2");
            //turn
            transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);

            if (Input.GetButtonDown("Fire2"))
            {
                //Destory bee's
                beeSpawner.destroyBees(transform.position, destroyRadius);
            }
        }
        #region OldCode

        ///v2
        //Scale velocity (by duration)...
        //Vector2 move = velocity * Time.deltaTime;

        //...then move
        //transform.Translate(move);


        ///v1
        //Note , 1 = 1 metre per frame
        //transform.Translate(move);
        #endregion  
    }

    public void calculateSpeed()
    {
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
            MovePlayerVertically();
        }

        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
            MovePlayerVertically();
        }

        else /* Brake */
        { 
            if (speed > 0)
            {

                //clamp
                speed = speed - brake * Time.deltaTime;

                //brake towards 0
                speed = Mathf.Clamp(speed, 0, maxSpeed);
                MovePlayerVertically();
            }
            else if (speed < 0)
            {
                //Brake
                speed = speed + brake * Time.deltaTime;

                //brake towards 0
                speed = Mathf.Clamp(speed, -maxSpeed, 0);

                MovePlayerVertically();
            }
        }

    }

    public void MovePlayerVertically()
    {
        //Speed Clamp
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        //scale velocity (direction * speed)
        Vector2 velocity = Vector2.up * speed;

        //move the object -> velocity * Time(by frame)
        transform.Translate(velocity * Time.deltaTime);
    }
}


