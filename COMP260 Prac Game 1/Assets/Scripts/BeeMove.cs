﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour
{
    #region Public
    public Transform target;
    public ParticleSystem explosionPrefab;

    public Transform Player1Target;
    public Transform Player2Target;

    public GameObject player1_go;
    public GameObject player2_go;

    #endregion

    #region Private
    private Vector2 heading = Vector2.right;

    private float speed;     //metre/s
    private float turnSpeed; //Degree/s
    private float angle;
    #endregion

    #region Randomness Parameters
    public float minSpeed = 0.5f, maxSpeed = 5f;
    public float minTurnSpeed = 10f, maxTurnSpeed = 30f;



    #endregion


    void Awake()
    {
        heading = Vector2.right;
        angle = Random.value * 360;
        heading = heading.Rotate(angle);

        //set speed and TurnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 Beepos = transform.position;
        player1_go = GameObject.Find("Player1");
        player2_go = GameObject.Find("Player2");

        float p1_dist = Vector2.Distance(Beepos, player1_go.transform.position);
        float p2_dist = Vector2.Distance(Beepos, player2_go.transform.position);

        if (p1_dist < p2_dist)
        {
            target = Player1Target;
        }
        else
        {
            target = Player2Target;
        }

        Vector2 direction = target.position - transform.position;

        //Calc turn per frame
        angle = turnSpeed * Time.deltaTime;

        //turn left or right
        if (direction.IsOnLeft(heading))
        {
            //target = left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            //target = right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        //Move by direction and distance + fix frame issue
        transform.Translate(heading * speed * Time.deltaTime);

        #region Oldcode


        ///V1
        ////get vector from bee -> target
        //Vector2 direction = target.position - transform.position;
        ////Normalise direction
        //direction = direction.normalized;
        //
        ////convert speed to a vector2 value
        //Vector2 velocity = direction * speed;
        //transform.Translate(velocity * Time.deltaTime);

        #endregion  
    }

    void OnDestroy()
    {
        //Particle at beePos, upon Destruction
        ParticleSystem explosion = Instantiate(explosionPrefab);
        //Position of explosion is position of (this).transform.position
        explosion.transform.position = transform.position;

        //Remove Particle once finished
        Destroy(explosion.gameObject, explosion.main.duration - 0.8f);
    }

    void OnDrawGizmos()
    {
        //draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        //draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

}
